exports.baseController = "base";
$.model = {
    "window" : {
        "title" : "Red Window",
        "button" : "submit",
        "colour" : "red"
    },
    "username" : "paulryan",
    "emailaddress" : "paul.ryan@piota.co.uk",
    "twittername" : "@sharpred"
};
$.bind();
// jshint unused:false
function openWindow(e) {
    var win3 = Alloy.createController('bluewin', $.model).getView();
    $.win1.openWindow(win3);
}
$.win2.setAccessibilityLabel("thususatest");
console.log("accessibility:" + $.win2.getAccessibilityLabel());
$.win1.open();
