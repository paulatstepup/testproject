var args = arguments[0] || {},
    nano = require("nano");
nano.syntax(/\#\#(.+?)\#\#/gi);
$.model = args;
$.bind = function() {
    nano($, $.model);
};
$.apply = function() {
    nano.apply();
};
