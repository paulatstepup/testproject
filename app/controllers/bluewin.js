exports.baseController = "base";
var args = arguments[0] || {};
args.window.title = "Blue Window";
args.window.button = "close";
args.window.colour = "blue";
//base controller assigns $model to args, so no need to set $.model
$.bind();
// jshint unused:false
function closeWindow() {
    //changes to the model can be made in code and then applied using $.apply from basecontroller
    $.model.window.title = "Red Window";
    $.model.window.button = "submit";
    $.model.window.colour = "red";
    $.apply();
    $.win3.close();
}